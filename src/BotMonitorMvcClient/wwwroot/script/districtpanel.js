﻿//Обработчики на нажатие по строке таблицы

function changeCurrentDistrictEvent(e) {
    console.log('Смена текущего дистрикта на ' + e.currentTarget.cells[0].firstChild.data);
    changeCurrentDistrict(e.currentTarget.cells[0].firstChild.data);
    refreshActualDistrictPanel();
    $('#districtTab').click();
}

function changeCurrentDistrict(districtname) {
    Cookies.set('district', districtname);

}
//Загрузка информации для панели бота
function refreshActualDistrictPanel() {
    var currentBotName = Cookies.get('district');
    if (currentBotName == undefined) {
        currentBotName = "Standart";
        changeCurrentDistrict(currentBotName);
    }
    return refreshDistrictPanel(currentBotName);
   // loadDistrictCommand();
}

function refreshDistrictPanel(botname) {
    //Запрос к методу загрузки информации 
    console.log("Обновление данных");
    return loadDistrictData(botname);
}

function loadDistrictData(botname) {
    //Запрос к методу загрузки информации 
    return $.get("Home/DistrictData", function (data) {
        var html = $.templates("#districtdatapanelTemplate").render(jQuery.parseJSON(data));
        $("#districtdatapanel").html(html);
        hookHandleOnSessionRow();
    });

}

function hookHandleOnSessionRow() {
    console.log("Попытка присоединить обработчики");
    $('tr[name=sessionRow]').click(changeCurrentSessionEvent);
}


//Обновление при измении дистрикта или при выполнении команды и получении ответа
function loadDistrictCommand() {
    //Запрос к методу загрузки информации о сервисах уровня дистрикта
    $.get("Home/DistrictCommand", function(data) {
        var html = $.templates("#districtcommandpanelTemplate").render(jQuery.parseJSON(data));
        $("#districtcommandpanel").html(html);
        $('#refreshdistrictcommandpanel').click(loadBotCommand);
    });

}
﻿//Обработчики на нажатие по строке таблицы

function changeCurrentSessionEvent(e) {
    console.log('Смена текущего дистрикта на ' + e.currentTarget.cells[0].firstChild.data);
    changeCurrentSession(e.currentTarget.cells[0].firstChild.data);
    var promise = refreshActualSessionPanel();
    if (promise != null) {
        promise.done(loadSessionCommand().done($('#sessionTab').click()));
    }
   // loadSessionCommand();
   ;

}

function changeCurrentSession(sessionId) {
    Cookies.set('session', sessionId);

}
//Загрузка информации для панели бота
function refreshActualSessionPanel() {
    var currentSessionId = Cookies.get('session');
    if (currentSessionId == undefined) {
        return null;
    }
    return refreshSessionPanel(currentSessionId);

}

function refreshSessionPanel(sessionId) {
    //Запрос к методу загрузки информации 
    console.log("Обновление данных сессии");
    return loadSessionData(sessionId);

}

function loadSessionData() {
    //Запрос к методу загрузки информации 

    return $.get("Home/SessionData", function (data) {
        data.h = Math.floor(data.timeLeft / 3600);
        data.m = Math.floor(data.timeLeft % 3600 / 60);
        var html = $.templates("#sessiondatapanelTemplate").render(data);
        $("#sessiondatapanel").html(html);   
    });
}

function loadSessionCommand() {
    //Запрос к методу загрузки информации о сервисах уровня дистрикта
    return $.get("Home/SessionCommand", function (data) {
        var html = $.templates("#sessioncommandpanelTemplate").render(jQuery.parseJSON(data));
        $("#sessioncommandpanel").html(html);

        $("#switchcontrol").click({value :"switchcontrol"},executeCommand);
    });

  
}


//Команды 

function executeCommand(event) {
    //Вызов команды "command"
    var datas = { command: event.data.value };
    $.post("Home/ExecuteSwitchMControlCommand", datas, function (data) {
      
    })
  .done(function () {
    
  })
  .fail(function (textStatus) {
      alert("Error execute command: " + command + textStatus);
  })
  .always(function () {
            loadSessionCommand();
        });
   
}


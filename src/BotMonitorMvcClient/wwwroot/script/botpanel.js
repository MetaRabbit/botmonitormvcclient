﻿
function changeCurrentBotEvent(e) {
    console.log('Смена текущего бота на '+e.target.text);
    changeCurrentBot(e.target.text);
    refreshActualBotPanel();
    loadBotCommand();

}
function changeCurrentBot(botname) {
    Cookies.set('botname', botname);
}

//Загрузка информации для панели бота
function refreshActualBotPanel() {
    var currentBotName = Cookies.get('botname');
    if (currentBotName == undefined) {
        currentBotName = "Exilebuddy";
        changeCurrentBot(currentBotName);
    }
    return refreshBotPanel(currentBotName);
}
function refreshBotPanel(botname) {
    //Запрос к методу загрузки информации 

    return loadBotData(botname);
}
function loadBotData(botname) {
    //Запрос к методу загрузки информации 
    return $.get("Home/BotData", function(data) {
        var html = $.templates("#botdatapanelTemplate").render(jQuery.parseJSON(data));
        $("#botdatapanel").html(html);
        hookHandleOnRow();
    }); 

}

function hookHandleOnRow() {
    $('tr[name=districtRow]').click(changeCurrentDistrictEvent);
}

function handleRowClick(e) {


    //Смена закладки
    $('#districtTab').click();
}

//Обновление при измении типа бота или при выполнении команды и получении ответа
function loadBotCommand() {
    //Запрос к методу загрузки информации о дискриктах
    $.get("Home/BotCommand", function(data) {

        var html = $.templates("#botcommandpanelTemplate").render(jQuery.parseJSON(data));
        $("#botcommandpanel").html(html);
        $('#refreshBotCommandPanel').click(loadBotCommand);
    });

}


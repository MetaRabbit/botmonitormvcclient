﻿function showDialogCreateProfile(id) {
        var dialog = $(id).data('dialog');
        dialog.open();
    }

function closeDialogCreateProfile(id) {
    var dialog = $(id).data('dialog');
    dialog.close();
}
function notifyOnErrorInput(input){
        var message = input.data('validateHint');
        $.Notify({
            caption: 'Error',
            content: message,
            type: 'alert'
        });
    }

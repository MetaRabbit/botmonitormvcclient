﻿function Room(id,name,users,map,scenario,idGroupChat) {
    this.Id = ko.observable(id);
    this.Name = ko.observable(name);
    this.Map = ko.observable(map);
    this.Scenario = ko.observable(scenario);
    this.Users = ko.observableArray(users);
    this.IdGroupChat = ko.observable(idGroupChat);
}
var defaultData = {
    users: [
    {
        login:"",
        id:""
    }],
    id: "",
    name: "",
    map: "",
    scenario: "",
    idGroupChat:""
};
function RoomsViewModel() {

    var self = this;

    this.Rooms = ko.observableArray([]);
    this.CurrentRoom = ko.mapping.fromJS(defaultData);
    this.CurrentRoomId = '',

    this.GetRoom = function(idRoom) {
        $.getJSON("/api/Room/" + idRoom, function(data) {
            var mappedTasks = $.map(data, function(item) {
                console.log(data.length);
                return new Account(item.Id, item.Name);
            });
            self.Accounts(mappedTasks);

        }).done(function() {

        });;
    }
    this.GetCurrentRoom = function() {
        $.getJSON("/api/Room/" + self.CurrentRoomId, function (data) {
            //var mappedUsers = $.map(data.users, function(item) {
            //    console.log(data.users.length);
            //    return new Account(item.id, item.login, item.login);
            //});

           // console.log(data);
            ko.mapping.fromJS(data, {}, self.CurrentRoom);
          
            //console.log(self.CurrentRoom.users()[0]);
        }).done(function() {

        });
    }
}
function GeneralViewModel() {
    var self = this;
    this.Rooms = new RoomsViewModel();
    this.Chat = new WordlChatViewModel();
    this.AccountView = null;
    this.Account = {
            idRoom:''
        },

    this.GetAccountByAjax = function () {
        $.getJSON("/Account/GetActiveAccount", function (data) {

            self.AccountView = ko.mapping.fromJS(data);
            Account.idRoom = data['idRoom'];
            self.Rooms.CurrentRoomId = data['idRoom'];
            
        }).done(function () {

        });;

    };


}
function isNotMax(e) {
    e = e || window.event;
    var target = e.target || e.srcElement;
    var code = e.keyCode ? e.keyCode : (e.which ? e.which : e.charCode);

    switch (code) {
        case 13:
        case 8:
        case 9:
        case 46:
        case 37:
        case 38:
        case 39:
        case 40:
            return true;
    }
    return target.value.length <= target.getAttribute('maxlength');
}
//global variables
var toUserLogin = "";


//
$(document).ready(function () {

    //var viewmodel = new GeneralViewModel();
    //setTimeout(function () {
    //    $.getJSON("/Account/GetActiveAccount", function (data) {
    //        viewmodel.Account.idRoom = data['idRoom'];
    //        console.log(viewmodel.Account.idRoom);
    //    }).done(function () {

    //        $.getJSON("/api/Room/", function (data) {

    //            viewmodel.Rooms = new RoomsViewModel(data);
    //            console.log(viewmodel.Rooms.CurrentRoom);
    //            //console.log(Account.idRoom);
    //        }).done(function () {
    //            setInterval(viewmodel.GetAccountByAjax, 6000);

    //            setInterval(viewmodel.Rooms.GetCurrentRoom, 7000);
               
    //        });;

    //    });;

  
    var container = $("#chatContainer");
    var refreshComponent = function () {
        $.get("Screen/Chat", function (data) { container.html(data); });
    };

    $(function () { window.setInterval(refreshComponent, 2000); });

    $('#formSendMessage').on('submit', function () {
        var id = this.name;
    //you have the form in JSON format
        var data = ko.toJSON(this,id);
        $.ajax({
            type: "POST",
            url: "Screen/SendMessage",
            data: data,
            success: function (msg) {
                // Do something interesting here.
            }
        });

    });
    //}, 400);
   
    //ko.applyBindings(viewmodel);
    
   // setInterval(viewmodel.GetAccountByAjax, 6000);

    //setInterval(viewmodel.Rooms.GetCurrentRoom, 7000);

    //setInterval(viewmodel.Chat.GetMessagesByAjax, 7000);
   
    

});

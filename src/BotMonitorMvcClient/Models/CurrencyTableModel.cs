﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class CurrencyTableModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}

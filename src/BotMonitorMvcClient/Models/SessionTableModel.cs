﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class SessionTableModel
    {

        public SessionTableModel()
        {
            BAgentState = "";
            Services = new List<BotAgentService>();
        }

        public int Id { get; set; }
        public string BotState { get; set; }
        public string GameState { get; set; }
        public string LogState { get; set; }
        public string BAgentState { get; set; }
        public string District { get; set; }
        public bool ManualControl { get; set; }
        public bool IsNeedUpdate { get; set; }
        public long TimeLeft { get; set; }
        public bool Registered { get; set; }
        public List<BotAgentService> Services;

    }
}

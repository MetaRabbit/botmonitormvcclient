﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class BotAgentServiceSettings
    {

        public BotAgentServiceSettings()
        {
            AdvanceItems = new List<string>();
        }

        public bool? AutoStart { get; set; }
        public List<string> AdvanceItems { get; set; }
    }
}

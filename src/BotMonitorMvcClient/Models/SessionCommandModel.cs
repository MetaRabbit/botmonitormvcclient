﻿using BotMonitorMvcClient.Models.BAgentService;

namespace BotMonitorMvcClient.Models
{
    public class SessionCommandModel
    {
        public SessionCommandModel()
        {
            ReloadService = new ReloadService();
        }

        public string Name { get; set; }
        public ReloadService ReloadService { get; set; }
    }
}

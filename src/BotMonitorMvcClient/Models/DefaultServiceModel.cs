﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class DefaultServiceModel
    {
        public string Name { get; set; }
        public bool Enable { get; set; }
        public int TotalItems { get; set; }
        public int FreeItems { get; set; }
        public int BadItems { get; set; }

    }
}

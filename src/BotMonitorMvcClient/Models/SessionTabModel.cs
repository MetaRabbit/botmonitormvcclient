﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class SessionTabModel
    {
        public SessionTabModel()
        {
            CurrencyTableModels = new List<CurrencyTableModel>();
        }

        public long TimeLeft { get; set; }
        public bool MControl { get; set; }
        public bool Registered { get; set; }
        public int Level { get; set; }
        public string Location { get; set; }
        public string CharName { get; set; }
        public List<CurrencyTableModel> CurrencyTableModels { get; set; } 
    }
}

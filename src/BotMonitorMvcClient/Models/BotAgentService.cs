﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class BotAgentService
    {

        public string Name { get; set; }
        public bool Running { get; set; }
        public bool Ready { get; set; }
        public BotAgentServiceSettings Settings { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class DistrictCommandModel
    {
        public DistrictCommandModel()
        {
            Services = new List<DefaultServiceModel>();
        }

        public string Name { get; set; }
        public List<DefaultServiceModel> Services { get; set; }
    }
}

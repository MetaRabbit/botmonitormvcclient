﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotMonitorMvcClient.Models
{
    public class BotCommandModel
    {
        public BotCommandModel()
        {
            //test
            //KeysService = new DefaultServiceModel()
            //{
            //    BadItems = 12,
            //    Enable = true,
            //    FreeItems = 1,
            //    TotalItems = 4,
            //    Name = "Key Service"
            //};
            Services = new List<DefaultServiceModel>();
        }

        public string Name { get; set; }
        public List<DefaultServiceModel> Services { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;

namespace BotMonitorMvcClient.Utils
{
    public static class CookieHelper
    {
        public static string GetBotNameOrDefault(this IRequestCookieCollection requestCookieCollection)
        {
            var botname = requestCookieCollection["botname"] ?? "Exilebuddy";
            return botname;
        }

        public static string GetDistrictNameOrDefault(this IRequestCookieCollection requestCookieCollection)
        {
            var botname = requestCookieCollection["district"] ?? "Standart";
            return botname;
        }

        public static string GetSession(this IRequestCookieCollection requestCookieCollection)
        {
            var botname = requestCookieCollection["session"] ?? null;
            return botname;
        }
    }
}

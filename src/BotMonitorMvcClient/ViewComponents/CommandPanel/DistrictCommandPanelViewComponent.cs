﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BotMonitorMvcClient.Models;
using BotMonitorMvcClient.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BotMonitorMvcClient.ViewComponents.CommandPanel
{
    public class DistrictCommandPanelViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {

            if (string.IsNullOrEmpty(HttpContext.Authentication.GetTokenAsync("access_token").Result))
            {
                return View(new DistrictCommandModel());
            }

            using (var httpClient = new HttpClient())
            {

                    //var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                    //var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                    //httpClient.BaseAddress = new Uri("http://localhost:60523");
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Authentication.GetTokenAsync("access_token").Result);

                    //var states = await httpClient.GetAsync($"api/DistrictService?botname={botname}&district={districtName}");
                    //var jsonContent = JsonConvert.DeserializeObject<DistrictCommandModel>(states.Content.ReadAsStringAsync().Result);

                    return View();
                }
               
              
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BotMonitorMvcClient.Models;
using BotMonitorMvcClient.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace BotMonitorMvcClient.ViewComponents.DataPanel
{
    public class BotDataPanelViewComponent : ViewComponent
    {

        public BotDataPanelViewComponent()
        {
         
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {

            //var token = await HttpContext.Authentication.GetTokenAsync("access_token");
            //using (var httpClient = new HttpClient())
            //{

            //    var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
            //    httpClient.BaseAddress = new Uri("http://localhost:60523");
            //    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            //    var states = await httpClient.GetAsync("api/Districs?botname=" + botname);
            //    var jsonContent = JsonConvert.DeserializeObject<List<DistrictTableModel>>(await states.Content.ReadAsStringAsync());

                return View();
            
          
        }
    }
}

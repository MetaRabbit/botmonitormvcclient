﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BotMonitorMvcClient.Models;
using BotMonitorMvcClient.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BotMonitorMvcClient.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
      
        public HomeController()
        {
   
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        public async Task<string> BotData()
        {
            var token = await HttpContext.Authentication.GetTokenAsync("access_token");
            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var states = await httpClient.GetAsync("api/Districs?botname=" + botname);
               // var jsonContent = JsonConvert.DeserializeObject<List<DistrictTableModel>>(await states.Content.ReadAsStringAsync());

                return await states.Content.ReadAsStringAsync();
            }

        
        } 

        public async Task<string> BotCommand()
        {

            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    HttpContext.Authentication.GetTokenAsync("access_token").Result);

                var states = await httpClient.GetAsync("api/BotService?botname=" + botname);
                //var jsonContent =
                //    JsonConvert.DeserializeObject<BotCommandModel>(states.Content.ReadAsStringAsync().Result);

                return await states.Content.ReadAsStringAsync();

            }

        }

        public async Task<string> DistrictData()
        {
            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Authentication.GetTokenAsync("access_token").Result);

                var states = await httpClient.GetAsync($"api/Session?botname={botname}&district={districtName}");
               // var jsonContent = JsonConvert.DeserializeObject<List<SessionTableModel>>(states.Content.ReadAsStringAsync().Result);

                return await states.Content.ReadAsStringAsync();
            }
          
                                  
        }

        public async Task<string> DistrictCommand()
        {
            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Authentication.GetTokenAsync("access_token").Result);

                var states = await httpClient.GetAsync($"api/DistrictService?botname={botname}&district={districtName}");
                //var jsonContent = JsonConvert.DeserializeObject<DistrictCommandModel>(states.Content.ReadAsStringAsync().Result);

                return await states.Content.ReadAsStringAsync();
            }
        }

        public async Task<IActionResult> SessionData()
        {

            if (string.IsNullOrEmpty(HttpContext.Authentication.GetTokenAsync("access_token").Result))
            {
                return View(new List<SessionTableModel>());
            }
            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                var sessionId = HttpContext.Request.Cookies.GetSession();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return View("Default", new SessionTabModel()
                    {
                        Location = "Session do not choose",
                        Level = 777,
                        TimeLeft = 2000

                    });
                }
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Authentication.GetTokenAsync("access_token").Result);

                SessionTabModel sessionTabModel = new SessionTabModel();
                //Запрос игровой информации
                var gameState = await httpClient.GetAsync($"api/GameInfo/{sessionId}?botname=exilebuddy");
                var jsonGameState = JObject.Parse(gameState.Content.ReadAsStringAsync().Result);
                //Заполнение игровой информации
                sessionTabModel.Level = jsonGameState["CurrentLevel"].ToObject<int>();
                sessionTabModel.Location = jsonGameState["CurrentAreaName"].ToObject<string>();
                sessionTabModel.CharName = jsonGameState["CharName"].ToObject<string>();


                foreach (var VARIABLE in jsonGameState["ValuableCurrency"].Children())
                {
                    sessionTabModel.CurrencyTableModels.Add(JsonConvert.DeserializeObject<CurrencyTableModel>(VARIABLE.ToString()));
                }

                //Запрос информации о состоянии ботагента

                var botAgentState = await httpClient.GetAsync($"api/Session/{sessionId}?botname={botname}&district={districtName}");
                var jsonContent = JObject.Parse(await botAgentState.Content.ReadAsStringAsync());

                if (jsonContent != null)
                {
                    jsonContent.Add("TimeLeft", sessionTabModel.TimeLeft);
                    sessionTabModel.TimeLeft = jsonContent["timeLeft"].ToObject<long>();
                    return Json(sessionTabModel);
                }
                else
                {
                    return Json(sessionTabModel);
                }
            }
    }

        public async Task<string> SessionCommand()
        {
            if (string.IsNullOrEmpty(HttpContext.Authentication.GetTokenAsync("access_token").Result))
            {
                return "";
            }

            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                var sessionId = HttpContext.Request.Cookies.GetSession();
                if (string.IsNullOrEmpty(sessionId))
                {
                    return "";
                }
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    HttpContext.Authentication.GetTokenAsync("access_token").Result);

                var botAgentState =
                    await httpClient.GetAsync($"api/Session/{sessionId}?botname={botname}&district={districtName}");
                var jsonContent =
                    JsonConvert.DeserializeObject<SessionTableModel>(botAgentState.Content.ReadAsStringAsync().Result);

                return await botAgentState.Content.ReadAsStringAsync();

                //if (jsonContent == null)
                //{
                //    return View("Default", new SessionTableModel()
                //    {
                //        Id = 777,


                //    });
                //}

            }
        }

        [HttpPost]
        public void ExecuteSwitchMControlCommand(string command)
        {
            if (command == null)
            {
                Response.StatusCode = 404;
                return;
            }
            using (var httpClient = new HttpClient())
            {

                var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();
                var districtName = HttpContext.Request.Cookies.GetDistrictNameOrDefault();
                var sessionId = HttpContext.Request.Cookies.GetSession();
                httpClient.BaseAddress = new Uri("http://localhost:60523");
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    HttpContext.Authentication.GetTokenAsync("access_token").Result);
                HttpContent content = new StringContent(JsonConvert.SerializeObject(new
                {
                    Command = command

                }),Encoding.UTF8, "application/json");

                var states =
                    
                        httpClient.PostAsync(
                            $"api/BotAgentCommand/{sessionId}?botname={botname}&district={districtName}", content).Result;


                if (!states.IsSuccessStatusCode)
                {
                    Response.StatusCode = 404;
                }
            }
        }

      
    }
}

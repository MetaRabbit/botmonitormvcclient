﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BotMonitorMvcClient.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace BotMonitorMvcClient.Controllers
{
    public class AdminPanelController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadPlugins(IList<IFormFile> files)
        {
            if (files == null || !files.Any())
            {
                ViewBag.Result = "File Is Empty :(";
                return View("Index"); ;
            }
            var botname = HttpContext.Request.Cookies.GetBotNameOrDefault();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60523");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                     HttpContext.Authentication.GetTokenAsync("access_token").Result);
                var file = files.FirstOrDefault();
                using (var content = new MultipartFormDataContent())
                {
                    byte[] body;
                    using (var stream = file.OpenReadStream())
                    {
                        body = new byte[stream.Length];
                        await stream.ReadAsync(body, 0, Convert.ToInt32(stream.Length));

                        var fileContent = new ByteArrayContent(body);

                        //fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                        fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = "Plugins.zip",

                        };
                        content.Add(fileContent, "Plugins.zip", "Plugins.zip");

                        var requestUri = $"api/Plugins/{botname}";
                        HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, requestUri);
                        httpRequestMessage.Content = fileContent;
                        try
                        {
                            var result = await client.SendAsync(httpRequestMessage);
                            ViewBag.Result = "Success";
                        }
                        catch (Exception e)
                        {
                            ViewBag.Result = e.InnerException;
                        }


                    }
                }
            }
            return View("index");
        }
        [HttpPost]
        public async Task<ActionResult> UploadBotAgent(IList<IFormFile> files)
        {
            if (files == null || !files.Any())
            {
                ViewBag.Result = "File Is Empty :(";
                return View("Index"); ;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60523");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                     HttpContext.Authentication.GetTokenAsync("access_token").Result);
                var file = files.FirstOrDefault();
                using (var content = new MultipartFormDataContent())
                {
                    byte[] body;
                    using (var stream = file.OpenReadStream())
                    {
                        body = new byte[stream.Length];
                        await stream.ReadAsync(body, 0, Convert.ToInt32(stream.Length));

                        var fileContent = new ByteArrayContent(body);

                        //fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                        fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = "botagent.zip",

                        };
                        content.Add(fileContent, "botagent.zip", "botagent.zip");

                        var requestUri = $"api/BotAgent";
                        HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, requestUri);
                        httpRequestMessage.Content = fileContent;
                        try
                        {
                            var result = await client.SendAsync(httpRequestMessage);
                            ViewBag.Result = "Success";
                        }
                        catch (Exception e)
                        {
                            ViewBag.Result = e.InnerException;
                        }


                    }
                }
            }
            return View("Index");
        }
        [HttpPost]
        public async Task<ActionResult> UploadBotAgentSettings(IList<IFormFile> files)
        {
            if (files == null || !files.Any())
            {
                Response.StatusCode = 404;
                ViewBag.Result = "File Is Empty :(";
                return View("Index"); ;
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60523");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                     HttpContext.Authentication.GetTokenAsync("access_token").Result);
                var file = files.FirstOrDefault();
                using (var content = new MultipartFormDataContent())
                {
                    byte[] body;
                    using (var stream = file.OpenReadStream())
                    {
                        body = new byte[stream.Length];
                        await stream.ReadAsync(body, 0, Convert.ToInt32(stream.Length));

                        var fileContent = new ByteArrayContent(body);

                        //fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                        fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                        {
                            FileName = "version.txt",

                        };
                        content.Add(fileContent, "version.txt", "version.txt");

                        var requestUri = $"api/BotAgentSettings";
                        HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, requestUri);
                        httpRequestMessage.Content = fileContent;
                        try
                        {
                            var result = await client.SendAsync(httpRequestMessage);
                            ViewBag.Result = "Success";
                        }
                        catch (Exception e)
                        {
                            ViewBag.Result = e.InnerException;
                        }


                    }
                }
            }
            return View("Index");
        }
    }
}
